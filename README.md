 <p class="center"><img src="img/soundshell_githubMain.png" /></p>

# Soundshell 
For Victoria University Wellington


The Soundshell client is built to run on a Raspberry Pi Zero using the Google AIY 
voice kit.


### Soundshell_Client Configuration 

__Install AIY Image__

Install Google AIY Linux image to the Raspberry Pi's sd card.
[https://github.com/google/aiyprojects-raspbian/releases](https://github.com/google/aiyprojects-raspbian/releases)

After OS setup, on the Raspberry Pi:
copy/extract folder: "soundshell_client" to /home/pi/

__Install dependencies__

```bash
#Lib for reading spreadsheets in .xls format
pip3 install openpyxl
```

__Configure Google API__

Soundshell needs to communicate with the Google Cloud Speech API to convert voice to text. Follow link to generate .json credentials for the service: [https://aiyprojects.withgoogle.com/voice/#makers-guide-change-to-the-cloud-speech-api](https://aiyprojects.withgoogle.com/voice/#makers-guide-change-to-the-cloud-speech-api) Goto section labelled "Custom Voice User Interface" 

__Soundshell Services__

The following are soundshell specific services:

1. **preServerCheck.service** - checks for internet connection, otherwise launches debug mode.
2. **soundshell.service** - Main Soundshell service that launches soundshell.py 
3. **volume-control.service** - launches volume-control.py controls all input from the rotary encoder.
4. **rfcomm.service** - started from preServerCheck.service to open serial communication through bluetooth.

__System Modified Service__

1. **bluetooth.service** - This file had the following lines added or modified in the **[Service]** section:

```bash
ExecStart=/usr/lib/bluetooth/bluetoothd -C 
ExecStartPost=/usr/bin/sdptool add SP 
ExecStartPost=/bin/hciconfig hci0 piscan 
```

__Register and enable/disable services__

```bash
#cp or mv soundshell_client services to /lib/systemd/system/
sudo cp /home/pi/soundshell_client/{soundshell.service, preServerCheck.service} /lib/systemd/system/

#cp or mv rfcomm for opening serial communications to /etc/systemd/system/
sudo cp /home/pi/soundshell_client/rfcomm.service /etc/systemd/system/

sudo systemctl daemon-reload

#These services are disabled by default and are started by preServerCheck.sh on startup.
sudo systemctl disable soundshell.service
sudo systemctl disable rfcomm.service

#Services enabled by default
sudo systemctl enable preServerCheck.service
sudo systemctl enable volume-control.service

sudo reboot

```
### 

### Soundshell_Server Configuration 

I used a Google Compute Engine server with the following specs:

**Distro:** Ubuntu 18.04LTS - 4 vCPUs, 15 GB 

**Firewall rules:** Soundshell IP 

**Protocols and ports:** TCP port:55443


__Copy Soundshell server:__

Copy server files to ~/soundshell_server

Running serversetup.sh will setup and Ubuntu 18.04 server. Edit server
paths (soundshell_server.py location) in soundshell_server.service before running.


__Manually Install Dependencies and Configure:__

```bash

sudo apt-get update


# Spacy and Pip3 setup

sudo apt-get install python3-pip

pip3 install -U spacy

python3 -m spacy download en_core_web_md


# set 'server_ip' variable to server ip or domain name in soundshell_server.py

cd soundshell_server
vim soundshell_server.py

```
---

# Operating the Soundshell: 


## soundshell_client

When the Soundshell is powered on the service, **preServerCheck.service** is called. This service runs the file, **preServerCheck.sh** which checks to see if the Soundshell server is running and the appropriate ports are opened. If communication with the server can be established, the script then launches **soundshell.py**. If it can not contact the server it will start **rfcomm.service**. Rfcomm.service opens up bluetooth serial communications, so troubleshooting via a terminal session can be established.

Terminal communications via ssh or bluetooth can use the service commands: **Start, Stop, Status** to control and see the current states of the Soundshell's' services. 

**Example:**
```bash
#starts soundshell
service soundshell start

#shows current status of soundshell
service soundshell status

#stops soundshell
service soundshell stop

```

## Bluetooth Debug Mode
Pair with the Soundshell. The Soundshell will show up as 'AIY-3-Voice-Bonnet-XXXX' it may show as "connected" then lose connection, which is normal. The connection will stay connected after a serial port communications is established via the debug device's terminal. 

**To connect to debug on Ubuntu:**
```bash
#bind to serial to bluetooth after pairing
sudo rfcomm bind /dev/rfcomm0 <bluetooth MAC addr>

#connect to the soundshell terminal using screen
sudo screen /dev/rfcomm0 115200
```
**Soundshell Wiring Diagram:**
#+html: <p align="center"><img src="img/soundshell_bb.png" /></p>
