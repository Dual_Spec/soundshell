#!/bin/bash

#server depends
sudo apt-get update
sudo apt-get install python3-pip -y
pip3 install -U spacy
python3 -m spacy download en_core_web_md

sudo cp soundshell_server.service /lib/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable soundshell_service

#get ip address
addr=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')
echo Change soundshell_server.py server_ip variable to $addr 
echo Command to start server: systemctl start soundshell_server.service
echo May require reboot
