import spacy
import numpy as np

import types
import struct #for bytes to string conversion

from numpy import dot
from numpy.linalg import norm

nlp = spacy.load('en_core_web_md')

#TODO openpyxl loading method to keep data consistency between client and server
# text doc
doc = nlp(open("poetry_rawtext.txt").read())

#convert string to bytes for sending spacy analysis back to client.
'''
def convert_string_to_bytes(string):
    bytes = b''
    for i in string:
        bytes += struct.pack("B", ord(i))
    return bytes

def convert_bytes_to_string(byte_msg):
    client_message = byte_msg.decode("utf-8")
    return client_message
'''

def sentvec(s):
    sent = nlp(s)
    return meanv([w.vector for w in sent])

sentences = list(doc.sents)

def spacy_closest_sent(space, input_str, n=1):
    input_vec = sentvec(input_str)
    return sorted(space,
                  key=lambda x: cosine(np.mean([w.vector for w in x], axis=0), input_vec),
                  reverse=True)[:n]

# cosine similarity
def cosine(v1, v2):
    if norm(v1) > 0 and norm(v2) > 0:
        return dot(v1, v2) / (norm(v1) * norm(v2))
    else:
        return 0.0

def meanv(coords):
    # assumes every item in coords has same length as item 0
    sumv = [0] * len(coords[0])
    for item in coords:
        for i in range(len(item)):
            sumv[i] += item[i]
    mean = [0] * len(sumv)
    for i in range(len(sumv)):
        mean[i] = float(sumv[i]) / len(coords)
    return mean
meanv([[0, 1], [2, 2], [4, 3]])

def process_client_message(message):
    for sent in spacy_closest_sent(sentences, message):
        print(sent.text)
        return sent.text
