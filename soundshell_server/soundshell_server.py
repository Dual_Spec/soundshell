# soundshell_server.py

#from sentance_analysis.py for spacy processing
from sentance_analysis import process_client_message

#SET SERVER IP ADDRESS. For Gcloud VM it is the internal ip addr
server_ip = "10.152.0.2"

def do_some_stuffs_with_input(input_string):  
    print("\n--Processing client input--")
    processed_message = process_client_message(input_string)
    return processed_message

def client_thread(conn, ip, port, MAX_BUFFER_SIZE = 4096):

    #decode bytes
    input_from_client_bytes = conn.recv(MAX_BUFFER_SIZE)

    # MAX_BUFFER_SIZE is how big the message can be
    # this is test if it's sufficiently big
    import sys
    siz = sys.getsizeof(input_from_client_bytes)
    if  siz >= MAX_BUFFER_SIZE:
        print("The length of input is probably too long: {}".format(siz))

    # decode input and strip the end of line
    input_from_client = input_from_client_bytes.decode("utf8").rstrip()

    res = do_some_stuffs_with_input(input_from_client)
    print("Result from client input: {} :is: {}".format(input_from_client, res))

    vysl = res.encode("utf8")  # encode the result string
    conn.sendall(vysl)  # send it to client
    conn.close()  # close connection
    print('Connection ' + ip + ':' + port + " ended")

def start_server():

    import socket
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # this is for easy starting/killing the app
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print('Socket created')

    try:
        #connecting to ip and port num
        soc.bind((server_ip, 55443))
        print('Socket bind complete')
    except socket.error as msg:
        import sys
        print('Bind failed. Error : ' + str(sys.exc_info()))
        sys.exit()

    #Start listening on socket
    soc.listen(10)
    print('Socket now listening')

    # for handling task in separate jobs we need threading
    from threading import Thread

    # this will make an infinite loop needed for 
    # not reseting server for every client
    while True:
        conn, addr = soc.accept()
        ip, port = str(addr[0]), str(addr[1])
        print('Accepting connection from ' + ip + ':' + port)
        try:
            #client_thread(conn, ip, port)
            Thread(target=client_thread, args=(conn, ip, port)).start()
        except:
            print("error")
            import traceback
            traceback.print_exc()
    soc.close()

start_server()
