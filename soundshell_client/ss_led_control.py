#!/usr/bin/env python3

#AIY board imports
#from aiy.leds import Leds, Color, Pattern
from aiy.pins import (PIN_A, PIN_B, PIN_C)
from gpiozero import LED, PWMLED
from gpiozero.tools import ramping_values, sin_values, scaled, inverted

#For LED flash times
from signal import pause

red = PWMLED(PIN_A)
blue = PWMLED(PIN_B)
green = PWMLED(PIN_C)


def ss_led_mode(led_operation):

    if led_operation == "green":
        red.off()
        green.on()
    elif led_operation == "red":
        green.off()
        red.on()
    elif led_operation == "flash":
        red.off()
        green.pulse()
    elif led_operation == "error":
        green.off()
        red.pulse()


#Different states here TODO add smoother states for interaction/frequency smooting
#Not Currently Called
def red_state():
    red.on()
    pause()


def green_state():
    green.pulse()
    pause()


def flash_green_state():
    green.pulse()
    pause()


def flash_red_state():
    red.pulse()
    pause()


#ss_led_mode("green")

