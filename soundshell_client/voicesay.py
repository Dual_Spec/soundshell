#!/usr/bin/env python3

#Voice utility that takes a string passed as an arg.
#Used during soundshell boot to say which mode it is booting into.
#Ex use: python3 voicesay.py "your string here"

#aiy voice import
import aiy.voice.tts
from sys import argv 

#Grab arg format string and pass it to the say function.
sysmessage = ''.join(argv[1:])
#print(sysmessage)
aiy.voice.tts.say(sysmessage)
