#This file will search for the string that is returned from the server in 
#specifed spreadsheet.  Then build a list of the poem's wav files to be played. 
#The first in the list will be the wav file that matches the search term 
#returned from the soundshell server.

import openpyxl #xlsx reading library
import random 

#Define spreadsheet file, and which sheet to work on.
wb = openpyxl.load_workbook('soundshell_audio_LUT.xlsx')
sheet = wb['voiceshell_audio_LUT']
##ss_str ="’s gold seal glows in the light" #Variable for testing lookups
matched_ranges = ()
filelist = [] #playlist to be sent to ss_audioplay

def search_sheet(search_str):
    print("SEARCH STR::", search_str.lstrip())
#find all filled rows plus one so search functions can find cell type 
#''None'' at the end of the sheet.
    numrows = sheet.max_row + 1

    #Scan colummn A for string from server
    for cell in sheet['A']:
        str_cell_value = str(cell.value)
        #print(str_cell_value)
        if str_cell_value.__contains__(search_str.strip('\n')):
            matched_row = cell.row


    #Find first line in poem range using search_str row# and iterating backward
    for row in range(matched_row, 0, -1):
        if (sheet.cell(row, 1).value is None):
            first_row = row + 1
            break
        if (sheet.cell(row,1).row == sheet.cell(1,1).row): 
            first_row = 1 #Statement for poem blocks if at begining of sheet.
            break

    #Find last line in poem range, using search string row# as starting point
    for row in range(matched_row, numrows+1):
        if (sheet.cell(row, 1).value is None):
            last_row = sheet.cell(row, 1).row
            break

    return matched_row, first_row, last_row


#Loop poem range and build wav file playlist
def create_playlist(match_ranges):
    #.wav file paths are colummn 'b' in soundshell_audio_LUT.xlsx
    print(match_ranges)
    filelist.clear()
    for val in range(match_ranges[1], match_ranges[2]):
        wav_cell = "b" + str(val)
        filelist.append(sheet[wav_cell].value)
        
    matched_text = sheet['b'+str(match_ranges[0])].value
    filelist.insert(0,matched_text)
    #print(filelist)
    return filelist


#This is specifically for screensaver error mode
#its late, needs refactoring, running out of time
def generate_random_poem():
    
    #finds a random line and then send that to make a playlist range
    numrows = sheet.max_row + 1
    rand_row = random.randint(1, numrows)
    while sheet.cell(rand_row, 1).value is None:
        rand_row = random.randint(1, numrows)
    else:
        matched_row = rand_row

    #Find first line in poem range using search_str row# and iterating backward
    for row in range(matched_row, 0, -1):
        if (sheet.cell(row, 1).value is None):
            first_row = row + 1
            break
        if (sheet.cell(row,1).row == sheet.cell(1,1).row): 
            first_row = 1 #Number for poem range if at begining of sheet.
            break

    #Find last line in poem range, using search string row# as starting point
    for row in range(matched_row, numrows+1):
        if (sheet.cell(row, 1).value is None):
            last_row = sheet.cell(row, 1).row
            break

    return matched_row, first_row, last_row


def make_screensaver_playlist():
    match_ranges = generate_random_poem()
    filepath_list = create_playlist(match_ranges)
    return filepath_list



#Calls both functions to read spreadsheet then populate filepathlist for player
def make_playlist(ss_str):
    match_ranges = search_sheet(ss_str)
    filepath_list = create_playlist(match_ranges)
    return filepath_list

##make_playlist(ss_str) #Call for testing variable lookups




