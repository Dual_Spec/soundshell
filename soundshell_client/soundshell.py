#!/usr/bin/env python3

#TODO refoctoring in error checking and perhaps stream capture.
#excepts should be more explicit

#Cloud speech imports
import argparse
import locale
import logging

#AIY kit specific imports
from aiy.board import Board, Led, Color
from aiy.cloudspeech import CloudSpeechClient
import aiy.voice.tts

#soundshell_client imports
import socket, random, time
from ss_audioplay import ss_get_playlist, ss_play_all, ss_play_matched, ss_get_screensaver
from ss_led_control import ss_led_mode

#WIFI error checking imports
import subprocess
import traceback

#WIFI error checking variables
WPA_CONF_PATH = '/etc/wpa_supplicant/wpa_supplicant.conf'
GOOGLE_SERVER_ADDRESS = ('speech.googleapis.com', 443)

ERROR_NOT_CONFIGURED='''
Please select and configure a WIFI network for normal operation.
'''

ERROR_NOT_CONNECTED='''
Please check to see if the WIFI is connected.
'''

ERROR_GOOGLE_SERVER='''
Failed to reach Google servers. WIFI is not connected.
'''

#Soundshell server address here:
SERVER_IP = "soundshell.vusd.nz"
SERVER_PORT = 55443
NUM_CONNS = 1

#Soundshell operating MODE
#MODE 1 = Normal question/query/answer mode, server comms are established
#MODE 2 = Screensaver mode, wifi/server, comms have failed
MODE = 1 #Default q/q/a mode, unless comms failures.

def get_hints(language_code):
    if language_code.startswith('en_'):
        return ('Tell me a detailed memory...')
    return None


#soundshell_client voice outputs
def rando_voice():
    intro_statements = ["That reminds me of this line in a poem,",
                        "I feel it's similar to the line in this poem,",
                        "I think that is much like this line in a poem I know,",
                     "That reminds me of this line from ay poem,"]
    return(intro_statements[random.randint(0,3)])


#soundshell_client voice outputs
def question_voice():
    intro_statements = ["  Would you like to hear the author recite the entire poem?",
                        "  Would you care to listen to the rest of the poem?",
                        "Shall I play the author reciting the rest of the poem?",
                        "Can I have the author recite the rest of the poem for you?"]
    return(intro_statements[random.randint(0,3)])


def locale_language():
    language, _ = locale.getdefaultlocale()
    return language


#Called when there is a network error and puts the soundshell into screensaver
def error_switch_mode():
    ss_led_mode("error")
    aiy.voice.tts.say("Please check wifi configuration. Defaulting to screensayver mode.")
    MODE = 2 #Set mode to screensaver
    screensaver_mode()


#Exemption check to see if network comms are working
def check_all_network_comms():
    try:
        setup_speech_client()
        wifi_check()
    except:
        error_switch_mode()

#call to the google API - set soundshell MODE if fail
def setup_speech_client():
    
    global MODE
    global parser
    global args
    global client
    global hints

    try:
        print("Checking Google connection...")
        logging.basicConfig(level=logging.DEBUG)
        parser = argparse.ArgumentParser(description='Assistant service example.')
        parser.add_argument('--language', default=locale_language())
        args = parser.parse_args()
        logging.info('Initializing for language %s...', args.language)
        client = CloudSpeechClient()
        hints = get_hints(args.language)
        print("Google connection should be working.")
    except:
        error_switch_mode()


def wifi_check():
    try:
        print("Checking WIFI connection...")

        if not check_wifi_is_configured():
            return

        if not check_wifi_is_connected():
            return

        print("WIFI should be working.")
    except:
        error_switch_mode()

#Main calls for the soundshell
def main():
    #Main MODE 1 listening and reply loop here
    if MODE == 1:
        with Board() as board:
            while True:
                #Idle stream cap will loop til it hears user input.
                print("Soundshell is in MODE: ", MODE)
                voice_text = idle_stream_capture()
                logging.info('You said: "%s"' % voice_text)
                ss_led_mode("flash")
                message = voice_text.lower()
                print(message)
                return_message = send_to_server(message)
                single_line_voice = ss_get_playlist(return_message)
                aiy.voice.tts.say(rando_voice())
                ss_play_matched()
                aiy.voice.tts.say(question_voice())#Ask to hear the rest of the poem
                
                #Reply stream capture will wait for 10 seconds then return to idle_stream_capture if no input.
                positive_replies = ['OK', 'yes', 'please', 'go ahead', 'lovely', 'sure', 'great']
                reply_text = reply_stream_capture()
                print("REPLY:", reply_text)
                reply = any(wrd in reply_text.lower() for wrd in positive_replies)
                if reply == True:
                    ss_play_all()
                elif reply == False:
                    ss_led_mode("flash")
                    aiy.voice.tts.say("Perhaps tell me another memory.")
                elif 'no_input' in reply_text.lower():
                    ss_led_mode("flash")
                    aiy.voice.tts.say("Could you tell me another memory.")
                    continue
    
    elif MODE == 2:
        error_switch_mode()


#Soundshell_server connections communication sockets
def send_to_server(message):
    try:
        soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        soc.settimeout(25)
        soc.connect((SERVER_IP, SERVER_PORT))
        print("setting up socket to send message")
        soc.send(message.encode("utf8")) #Encode the string to bytes  
        result_bytes = soc.recv(4096) # the number means how the response can be in bytes  
        result_string = result_bytes.decode("utf8", 'ignore') # the return will be in bytes, so decode
        #print("Result from server is {}".format(result_string))
        return result_string 
    except:
        error_switch_mode()


def screensaver_mode():
    while True:
        ss_led_mode("flash")
        ss_get_screensaver()
        ss_play_all()
        time.sleep(3)


#Main soundshell listening loop for poetry lookup
def idle_stream_capture():
    text = None
    with Board() as board:
        while text is None:
            if hints:
                logging.info('Waiting for INITAL input... ')
                ss_led_mode("green")
            else:
                logging.info('Say something.')
            text = client.recognize(language_code=args.language,
                                    hint_phrases=hints)
            if text is None:
                logging.info('No speech input')
                continue

        return text


#WProcesses the reply of the user
def reply_stream_capture():
    text = None
    with Board() as board:
        while text is None:
            if hints:
                logging.info('Waiting for REPLY input... ')
                ss_led_mode("flash")
            else:
                logging.info('Say something.')
            text = client.recognize(language_code=args.language, hint_phrases=hints)
            if text is None:
                logging.info('No speech input')
                text = "no_input"
                break

        return text


#WIFI error checking methods
def error(message):
    print(message.strip())


def check_wifi_is_configured():
    """Check wpa_supplicant.conf has at least one network configured."""
    output = subprocess.check_output(['sudo', 'cat', WPA_CONF_PATH]).decode('utf-8')
    if 'network=' not in output:
        error(ERROR_NOT_CONFIGURED)
        return False
    return True


def check_wifi_is_connected():
    """Check wlan0 has an IP address."""
    output = subprocess.check_output(['ifconfig', 'wlan0']).decode('utf-8')
    if 'inet ' not in output:
        error(ERROR_NOT_CONNECTED)
        return False
    return True


#Check all communications and handshake with google services, initalize soundshell.
check_all_network_comms()

if __name__ == '__main__':
    main()


