#!/bin/bash

#Checks soundshell server. If nc returns with connection, it launches the soundshell service.
#otherwise bluetooth debug service is launched.

#Tell the user what we are doing since there is a 30 sec delay now.
python3 /home/pi/soundshell_client/voicesay.py "Sound shell configuring network and connecting to server. Please Wait"

#service not waiting long enough for a true connection, so we sleep til pi internet connects. 
#Shit fix but it works.
sleep 15

#using netcat, if server true start soundshell, else start debug mode. suppressing stderr
if nc -z soundshell.vusd.nz 55443 2> /dev/null
then
	echo "Server OK, start Soundshell"
	python3 /home/pi/soundshell_client/voicesay.py "Server check OK, starting the Sound Shell"
	sudo service soundshell start
else
	echo "Server Error, starting bluetooth debug"
	python3 /home/pi/soundshell_client/voicesay.py "Server Error, starting blue tooth debug"
	sudo service bluetooth start
	sudo service rfcomm start
fi
