import time
from pygame import mixer
from ss_spreadsheet_lookup import make_playlist, search_sheet, make_screensaver_playlist

#Initalize audio mixer
mixer.init()

#test message
##ss_server_message = "Two men step out, silent in the silence, one from each side of the dark that wells in the dark of the pines."


#Call to make lookup the playlist
def ss_get_playlist(server_message):
    global playlist
    playlist = make_playlist(server_message)
    print(playlist)


def ss_get_screensaver():
    global playlist
    playlist = make_screensaver_playlist()
    

#Main basic playback functionality called from both playback functions
def ss_play_audio(playlist):
    s = mixer.Sound(playlist)
    s.play()
    wav_len = s.get_length() 
    time.sleep(wav_len) #Sleep allows the audio play for the proper length


#Plays all audio returned in list from spreadsheet
def ss_play_all():
    for song in range(1,len(playlist)): #Playlist starts at index 1
        poem_path = playlist[song]
        ss_play_audio(poem_path)


#Plays a single matched line from the spreadsheet
def ss_play_matched():
    poem_path = playlist[0] #Matched audio stored in list at index 0
    ss_play_audio(poem_path)

##ss_get_playlist(ss_server_message)
##ss_play_matched()
